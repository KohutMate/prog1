#include <stdio.h>
#include <stdlib.h>

int isValidEmail(char *email) {
    int i = 0;
    int kukac = 0;
    int pont = 0;

    if (email[0] == '@' || email[0] == '.') {
        return 0;
    }

    while (email[i] != '\0') {
        if (email[i] == '@') {
            kukac++;
            if (email[i + 1] == '.' || email[i - 1] == '.')
                return 0;

        } else if (email[i] == '.') {
            pont++;
            if (email[i + 1] == '@' || email[i - 1] == '@')
                return 0;

        }
        i++;
    }
    if (email[i - 1] == '@' || email[i - 1] == '.') {
        return 0;
    }

    if (kukac == 1 && pont >= 1) {
        return 1;
    } else {
        return 0;
    }
}

int main() {
    int result = isValidEmail("kohutmate61@gmail.com");

    if (result) {
        printf("Ervenyes email cim");
    } else {
        printf("Ervenytelen email cim");
    }
}
